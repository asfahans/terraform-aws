terraform {
    required_version = ">= 0.12"
    backend "s3" {
        bucket = "myasfahan-bucket"
        key = "myapp/state.tfstate"
        region = "ap-south-1"
    }
}

provider "aws" {
    region = "ap-south-1"
} 

resource "aws_vpc" "myapp-vpc" {
    cidr_block = var.vpc_cidr_block
    tags = {
        Name = "${var.env_prefix}-vpc"
    }
}

resource "aws_subnet" "myapp-subnet-1" {
    vpc_id = aws_vpc.myapp-vpc.id
    cidr_block = var.subnet_cidr_block
    availability_zone = var.avail_zone
    tags = {
        Name = "${var.env_prefix}-subnet-1"
    }
}

# Create custom route table
# resource "aws_route_table" "myapp_route_table" {
#     vpc_id = aws_vpc.myapp-vpc.id
#     route {
#         cidr_block = "0.0.0.0/0"
#         gateway_id = aws_internet_gateway.myapp_igw.id
#     }
#     tags = {
#         Name = "${var.env_prefix}-rtb"
#     }
# }

resource "aws_internet_gateway" "myapp_igw" {
    vpc_id = aws_vpc.myapp-vpc.id
    tags = {
        Name = "${var.env_prefix}-igw"
    }
}

resource "aws_default_route_table" "main-rtb" {
    default_route_table_id = aws_vpc.myapp-vpc.default_route_table_id

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.myapp_igw.id
    }
    tags = {
        Name = "${var.env_prefix}-main-rtb"
    }
}


# For custom route table ONLY
# resource "aws_route_table_association" "a-rtb-subnet" {
#     subnet_id = aws_subnet.myapp-subnet-1.id
#     route_table_id = aws_route_table.myapp_route_table.id
# }

//resource "aws_security_group" "myapp-sg" {
resource "aws_default_security_group" "default-sg" {                // incase we want to use the default security group
    //name = "myapp-sg"                                             // incase of default, name is not required
    vpc_id = aws_vpc.myapp-vpc.id

    ingress {
         from_port = 22
         to_port = 22
         protocol = "tcp"
         cidr_blocks = [var.my_ip]
    }

    ingress {
        from_port = 8080
        to_port = 8080
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
        from_port = 0                                               // no restriction 
        to_port = 0                                                 // no restriction
        protocol = "-1"                                             // no restriction for any protocol
        cidr_blocks = ["0.0.0.0/0"]
        prefix_list_ids = []                                        // for allowing access to VPC endpoints. Not very interesting
    }

    tags = {
        //Name = "${var.env_prefix}-sg"
        Name = "${var.env_prefix}-default-sg"                       // incase of default, changing Name to more appropriate one 
    }
}

data "aws_ami" "latest-amazon-linux-image" {
    most_recent = true
    owners = ["amazon"]
    filter {
        name = "name"
        values = ["amzn2-ami-hvm-*-x86_64-gp2"]
    }
    filter {
        name = "virtualization-type"
        values = ["hvm"]
    }
}

resource "aws_key_pair" "ssh-key" {
        key_name = "server-key"
        public_key = file(var.public_key_location)
}

resource "aws_instance" "myapp-server" {
    ami = data.aws_ami.latest-amazon-linux-image.id             // Mandatory
    instance_type = var.instance_type                           // Mandatory
    
    // Optional but required in our case
    subnet_id = aws_subnet.myapp-subnet-1.id
    vpc_security_group_ids = [aws_default_security_group.default-sg.id]
    availability_zone = var.avail_zone
    
    associate_public_ip_address = true
    key_name = aws_key_pair.ssh-key.key_name

    user_data = file("entry-script.sh")

    tags = {
        Name = "${var.env_prefix}-server"
    }
}

